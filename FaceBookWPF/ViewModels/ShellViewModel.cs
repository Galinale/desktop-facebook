﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Caliburn.Micro;

namespace FaceBookWPF.ViewModels
{
    public class ShellViewModel : Conductor<IPage>.Collection.OneActive, IHandle<string>
    {
        private readonly IViewModelFactory m_ViewModelFactory;
        private readonly IEventAggregator m_EventAggregator;

        public ShellViewModel(IViewModelFactory i_ViewModelFactory , IEventAggregator i_EventAggregator)
        {
            m_ViewModelFactory = i_ViewModelFactory;
            m_EventAggregator = i_EventAggregator;

            m_EventAggregator.Subscribe(this);
            ShowLoginPage();
        }

        public void Handle(string message)
        {
            switch(message)
            {
                case "Login":
                    ShowLoginPage();
                    break;

                case "Profile":
                    ShowProfilePage();
                    break;

                case "Collage":
                    ShowCollagePage();
                    break;
            }
        }

        private void ShowLoginPage()
        {
            ActivateItem(m_ViewModelFactory.CreateLoginViewModel());
        }

        private void ShowProfilePage()
        {
            ActivateItem(m_ViewModelFactory.CreateProfileViewModel());
            //ActivateItem(new RunningTextViewModel());
            
        }

        private void ShowCollagePage()
        {
            ActivateItem(m_ViewModelFactory.CreateCollageViewModel());
        }
    }


}

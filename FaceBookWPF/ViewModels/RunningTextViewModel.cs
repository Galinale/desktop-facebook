﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;
using System.Windows.Documents;
using Caliburn.Micro;
using DesktopFacebook;
using FaceBookWPF.Models;
using FacebookWrapper.ObjectModel;

namespace FaceBookWPF.ViewModels
{
    public class RunningTextViewModel : Screen , IPage
    {
        private List<string> m_Messages = new CircularList<string>();
        private IEnumerator<string> m_Iter;

        private readonly Timer m_Timer = new Timer(1500);

        public RunningTextViewModel()
        {
            IsEvents = true;
            m_Iter = m_Messages.GetEnumerator();

            m_Timer.AutoReset = true;
            m_Timer.Elapsed += (sender, e) =>
                                   {
                                       if (m_Iter != null && !m_Iter.MoveNext())
                                       {
                                           m_Iter = m_Messages.GetEnumerator();
                                       }
                                       Text = m_Iter.Current;
                                   };
            m_Timer.Start();
        }

        private string m_Text = "Test Run";
        public string Text
        {
            get
            {
                return m_Text;
            }
            private set
            {
                m_Text = value;
                NotifyOfPropertyChange(() => Text);
            }
        }

        private bool m_IsCheckins;
        public bool IsCheckins
        {
            get { return m_IsCheckins; }
            set
            {
                m_IsCheckins = value;
                if (m_IsCheckins)
                {
                    m_Messages = getCheckinsMessageList() as List<string>;
                    m_Iter = m_Messages.GetEnumerator();
                    m_IsEvents = false;
                    NotifyOfPropertyChange(() => m_IsCheckins);
                    NotifyOfPropertyChange(() => IsEvents);
                }

                NotifyOfPropertyChange(() => m_IsCheckins);
                NotifyOfPropertyChange(() => IsEvents);
            }
        }

        private bool m_IsEvents;
        public bool IsEvents
        {
            get { return m_IsEvents; }
            set
            {
                m_IsEvents = value;
                if (m_IsEvents)
                {
                    m_Messages = getEventMessageList() as List<string>;
                    m_Iter = m_Messages.GetEnumerator();
                    m_IsCheckins = false;
                    NotifyOfPropertyChange(() => m_IsCheckins);
                    NotifyOfPropertyChange(() => IsEvents);
                }
            }
        }

        private static IEnumerable<string> getCheckinsMessageList()
        {
            var checkinsList = new CircularList<string>();
            var text = string.Empty;

            if (UserData.LoggedInUser != null && UserData.LoggedInUser.Checkins.Count > 0)
            {
                foreach (Checkin checkin in UserData.LoggedInUser.Checkins)
                {
                    if (!string.IsNullOrEmpty(checkin.From.Name))
                    {
                        text = checkin.From.Name + " : ";
                    }

                    if (!string.IsNullOrEmpty(checkin.Message))
                    {
                        text = text + checkin.Message;
                    }

                    if (!string.IsNullOrEmpty(checkin.Place.Name))
                    {
                        text = text + ", in " + checkin.Place.Name;
                    }

                    if (!string.IsNullOrEmpty(checkin.CreatedTime.ToString()))
                    {
                        text = text + ", at " + checkin.CreatedTime.ToString();
                    }

                    checkinsList.Add(text);
                }
            }

            if (checkinsList.Count == 0) checkinsList.Add("No check-ins");

            return checkinsList;
        }

        private static IEnumerable<string> getEventMessageList()
        {
            var eventsList = new CircularList<string>();
            var text = string.Empty;

            foreach (Event FbEvent in UserData.LoggedInUser.Events)
            {
                if (!string.IsNullOrEmpty(FbEvent.Owner.Name))
                {
                    text = FbEvent.Owner.Name + " : ";
                }

                if (!string.IsNullOrEmpty(FbEvent.Owner.Name))
                {
                    text = text + "remember to attend " + FbEvent.Name + " ";
                }

                if (!string.IsNullOrEmpty(FbEvent.StartTime.ToString()))
                {
                    text = text + "on " + FbEvent.StartTime.ToString();
                }

                eventsList.Add(text);
            }

            if (eventsList.Count == 0) eventsList.Add("No events");

            return eventsList;
        }
    }

}

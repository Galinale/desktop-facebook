﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using FaceBookWPF.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FaceBookTests
{
    [TestClass]
    public class CircularListTests
    {
        [TestMethod]
        public void CircularList_circulatesSizePlusOneTimes_EnumeratorReturnfirstItemInList()
        {
            var list = new CircularList<int> {1, 2, 3};

            var enumerator = list.GetEnumerator();

            for (int i = 0; i < 4; i++)
            {
                enumerator.MoveNext();    
            }
            
            Assert.AreEqual(1 , enumerator.Current);
        }
    }
}

using System;
using Caliburn.Micro;
using FacebookWrapper.ObjectModel;

namespace FaceBookWPF.ViewModels
{
    public class PostViewModel : Screen, IPost
    {
        //Todo: change prop to member field
        private Post m_Post;

        public PostViewModel(Post i_Post)
        {
            m_Post = i_Post;
        }

        public string Id { get { return m_Post.Id; } }

        public string UserName { get { return m_Post.From.Name; } }      

        public string UserPicURL { get { return m_Post.From.PictureSmallURL; } }

        public string Caption { get { return m_Post.Caption; } }

        public string Message { get { return m_Post.Message; } }

        public string PostPicURL { get { return m_Post.PictureURL; } }

        public DateTime PublishingDate { get { return m_Post.CreatedTime ?? default(DateTime); } }

        public bool Like { get; set; }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using Caliburn.Micro;
using DesktopFacebook.Models;
using FaceBookWPF.ViewModels;
using FaceBookWPF.Views;
using Ninject;
using Ninject.Extensions.Factory;

namespace FaceBookWPF
{
    public class NinjectBootstrapper : Bootstrapper<ShellViewModel>
    {
        private IKernel m_Kernel;

        protected override void Configure()
        {
            m_Kernel = new StandardKernel(new Binder());

           
            //var eventAggregator = m_Kernel.Get<IEventAggregator>();
            //var shellViewModel = m_Kernel.Get<ShellViewModel>();
            //var loginViewModel = m_Kernel.Get<LoginViewModel>();
            //var profileViewModel = m_Kernel.Get<ProfileViewModel>();
            
            //eventAggregator.Subscribe(shellViewModel);
            //eventAggregator.Subscribe(loginViewModel);
            //eventAggregator.Subscribe(profileViewModel);

            //eventAggregator.Publish();
        }

        protected override object GetInstance(Type service, string key)
        {
            if (service != null)
            {
                return m_Kernel.Get(service);
            }

            throw new ArgumentNullException("service");
        }

        protected override IEnumerable<object> GetAllInstances(Type service)
        {
            return m_Kernel.GetAll(service);
        }

        protected override void BuildUp(object instance)
        {
            m_Kernel.Inject(instance);
        }

        protected override IEnumerable<Assembly> SelectAssemblies()
        {
            return new[] { Assembly.GetExecutingAssembly()
                //  TODO: see example http://caliburnmicro.codeplex.com/discussions/230861
                //, typeof(GuestModule).Assembly            
                //, typeof(AdminModule).Assembly 
            };
        }
    }
}


﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace DesktopFacebook
{
    //// Name: design.pattern
    ////
    ////Password: patternsdesign
    public class Config
    {
        public static readonly string AppID = "152259124970414";
        private static object m_InstanceKey = new object();

        private Config() 
        { 
        }

        private static Config m_instance = null;

        public static Config Instance
        {
            get
            {
                if (m_instance == null)
                {
                    lock (m_InstanceKey)
                    {
                        if (m_instance == null)
                        {
                            m_instance = Config.LoadOrCreate();
                        }
                    }
                }

                return m_instance;
            }
        }

        private static Config LoadOrCreate()
        {
            try
            {
                using (StreamReader reader = new StreamReader
                    (File.Open("DFacebok.config.dat", FileMode.Open)))
                {
                    return new XmlSerializer(typeof(Config)).Deserialize(reader) as Config;
                }
            }
            catch (Exception)
            {
                File.Delete("DFacebok.config.dat");
                return new Config();
            }
        }

        public string AccessToken { get; set; }

        public bool IsRememberMe { get; set; }

        public void Save()
        {
            string ConfigFileName = "DFacebok.config.dat";
            FileStream configFileStream = File.Exists(ConfigFileName) ?
                File.Open(ConfigFileName, FileMode.Truncate) :
                File.OpenWrite(ConfigFileName);

            using (StreamWriter writer = new StreamWriter(configFileStream))
            {
                ////TODO: trancate the file when opening it
                var xmlSerializer = new XmlSerializer(this.GetType());
                xmlSerializer.Serialize(writer, this);
            }
        }
    }
}
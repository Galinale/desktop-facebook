﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using FacebookWrapper.ObjectModel;

namespace FaceBookWPF.Models
{
    public static class Collage
    {
        public static string CreateCollageOfProfilePics(
            List<string> i_PicUrlList,
            int i_CollageWidth = 850, ////width of cover pic
            int i_CollageHieght = 315)////Hieght of cover pic
        {
            int collageArea = i_CollageHieght * i_CollageWidth;
            int friendArea = collageArea / i_PicUrlList.Count;
            int friendSize = Convert.ToInt32(Math.Sqrt(friendArea));
            string pictureFileName = GetNameForThePicture();

            var resizedBitmaps = new List<Bitmap>();

            foreach (Bitmap resizedBitmap in i_PicUrlList
                .Select(loadPictureAsBitmapFrom)
                .Select(picture => resizeBitmap(picture, friendSize, friendSize)))
            {
                resizedBitmaps.Add(resizedBitmap);
            }

            using (Bitmap collage = new Bitmap(i_CollageWidth, i_CollageHieght))
            {
                using (Graphics canvas = Graphics.FromImage(collage))
                {
                    int numOfColumns = i_CollageWidth / friendSize;

                    for (int friendsCounter = 0, row = 0; friendsCounter < i_PicUrlList.Count; row++)
                    {
                        for (int column = 0; column < numOfColumns; column++, friendsCounter++)
                        {
                            // Fail Safe
                            if (friendsCounter < i_PicUrlList.Count)
                            {
                                canvas.DrawImage(resizedBitmaps[friendsCounter], column * friendSize, row * friendSize);
                            }
                        }
                    }

                    canvas.DrawImage(collage, i_CollageWidth, i_CollageHieght);
                    collage.Save(pictureFileName, ImageFormat.Jpeg);
                }
            }

            return pictureFileName;
        }

        public static string CreateCollageOfProfilePics(
            this List<User> i_FriendsList,
            int i_CollageWidth = 850, ////width of cover pic
            int i_CollageHieght = 315)////Hieght of cover pic
        {
            int collageArea = i_CollageHieght * i_CollageWidth;
            int friendArea = collageArea / i_FriendsList.Count;
            int friendSize = Convert.ToInt32(Math.Sqrt(friendArea));
            string pictureFileName = GetNameForThePicture();

             var resizedBitmaps = new List<Bitmap>();

            foreach (Bitmap resizedBitmap in i_FriendsList
                .Select(friend => loadPictureAsBitmapFrom(friend.PictureNormalURL))
                .Select(picture => resizeBitmap(picture, friendSize, friendSize)))
            {
                resizedBitmaps.Add(resizedBitmap);
            }

            using (Bitmap collage = new Bitmap(i_CollageWidth, i_CollageHieght))
            {
                using (Graphics canvas = Graphics.FromImage(collage))
                {
                    int numOfColumns = i_CollageWidth / friendSize;

                    for (int friendsCounter = 0, row = 0; friendsCounter < i_FriendsList.Count; row++)
                    {
                        for (int column = 0; column < numOfColumns; column++, friendsCounter++)
                        {
                            // Fail Safe
                            if (friendsCounter < i_FriendsList.Count) 
                            {
                                canvas.DrawImage(resizedBitmaps[friendsCounter], column * friendSize, row * friendSize);
                            }
                        }
                    }

                    canvas.DrawImage(collage, i_CollageWidth, i_CollageHieght);
                    collage.Save(pictureFileName, ImageFormat.Jpeg);
                }
            }

            return pictureFileName;
        }

        private static string GetNameForThePicture()
        {
            string executingAssemblyPath = Assembly.GetExecutingAssembly().Location;
            string path = Path.GetDirectoryName(executingAssemblyPath);
            const string name = "collage";
            const string extension = "jpeg";
            int index = 0;

            string fileWithPath;
            do
            {
                index++;
                string fullFileName = string.Format("{0}{1}.{2}", name, index, extension);
                fileWithPath = Path.Combine(path, fullFileName);
            } 
            while (File.Exists(fileWithPath));

            return fileWithPath;
        }

        private static Bitmap loadPictureAsBitmapFrom(string i_Url)
        {
            HttpWebRequest wreq = null;
            HttpWebResponse wresp = null;
            Stream mystream = null;
            Bitmap bmp = null;

            try
            {
                wreq = (HttpWebRequest) WebRequest.Create(i_Url);
                wreq.AllowWriteStreamBuffering = true;
                wresp = (HttpWebResponse) wreq.GetResponse();

                mystream = wresp.GetResponseStream();
                if (mystream != null)
                {
                    bmp = new Bitmap(mystream);
                }
            }
            finally
            {
                if (mystream != null)
                {
                    mystream.Close();
                }

                if (wresp != null)
                {
                    wresp.Close();
                }
            }

            return bmp;
        }

        private static Bitmap resizeBitmap(Bitmap i_SourceBmp, int i_Width, int i_Height)
        {
            Bitmap result = new Bitmap(i_Width, i_Height);
            using (Graphics g = Graphics.FromImage(result))
            {
                g.DrawImage(i_SourceBmp, 0, 0, i_Height, i_Height);
            }

            return result;
        }
    }
}

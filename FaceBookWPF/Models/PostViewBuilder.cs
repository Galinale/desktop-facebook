﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace FaceBookWPF.Models
{

    public class PostViewBuilder
    {
        private Image m_Image       =null;
        private Image m_PostPic     =null;
        private TextBlock m_Name    =null;
        private TextBlock m_Date    =null;
        private TextBlock m_Message =null; 
        private TextBlock m_Caption =null;

        public void CreateProfilePic()
        {
            m_Image = new Image
                          {
                              Name = "UserPicURL",
                              Height = 53,
                              Width = 53,  
                              HorizontalAlignment = HorizontalAlignment.Left,
                              Margin= new Thickness(5,5,5,5)
                          };
        }

        public void CreateName()
        {
            m_Name = new TextBlock
                         {
                             Name = "UserName",
                             HorizontalAlignment = HorizontalAlignment.Left,
                             VerticalAlignment = VerticalAlignment.Top,
                             Margin= new Thickness(71,12,0,0),
                         };
        }

        public void CreateDate() //datetime
        {
            m_Date = new TextBlock
                         {
                             Name = "PublishingDate",
                             Height = 23 ,
                             Width = 120 ,
                             HorizontalAlignment = HorizontalAlignment.Left,
                             Margin= new Thickness(71,42,0,0)
                         };
        }

        public void CreateMessage()
        {
            m_Message = new TextBlock {Name = "Message"};
        }

        public void CreatePostPicture()
        {
            m_PostPic = new Image {Name = "PostPicURL"};
        }

        public void CreateCaption()
        {
            m_Caption = new TextBlock {Name = "Caption"};
        }

        public PostControl Build()
        {
            var userDetailGrid = new Grid(){Height = 70};
            userDetailGrid.Children.Add(m_Image);
            userDetailGrid.Children.Add(m_Name);
            userDetailGrid.Children.Add(m_Date);

            userDetailGrid.Children.Add(new Button()
                                            {
                                                Name = "Like",
                                                Content = new TextBlock(){Text = "Like"},
                                                HorizontalAlignment = HorizontalAlignment.Left,
                                                Width = 53,
                                                Height = 25 ,
                                                Margin= new Thickness(5,5,5,5)
                                            });


            var postControl = new PostControl
                                  {
                                      Margin = new Thickness(0, 0, 0, 15),
                                      Width = 500,
                                      MinWidth = 500,
                                      Children =
                                          {
                                              new Border
                                                  {
                                                      Width = 500,
                                                      Height = 3,
                                                      Background = Brushes.DodgerBlue,
                                                      // Set background here
                                                  },

                                              userDetailGrid,
                                              m_Message,
                                              m_PostPic,
                                              m_Caption                   

                                          }
                                  };    

            return postControl;
        }
    }

    public class PostControl : StackPanel
    {
        
    }
}

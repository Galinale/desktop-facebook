using System;
using System.Collections.Generic;
using System.Linq;
using FacebookWrapper;
using FacebookWrapper.ObjectModel;

namespace DesktopFacebook
{
    public static class UserData
    {
        public static User LoggedInUser { get; set; }

        //TODO: rework into method that return posts acording to input
        public static IEnumerable<Post> GetUserPosts()
        {
            return from post in UserData.LoggedInUser.Posts
                   where !string.IsNullOrEmpty(post.Message) ||
                          !string.IsNullOrEmpty(post.Caption) ||
                          !string.IsNullOrEmpty(post.PictureURL)
                   select post;
        }

        //TODO: throw exception if user is null
        public static Album GetAlbumByName(string i_AlbumName)
        {
            return (from album in LoggedInUser.Albums
                    where album.Name == i_AlbumName
                    select album).FirstOrDefault();
        }

        public static string GetCoverPhotoUrl()
        {
            const string c_CoverPhotos = "Cover Photos";
            var coverPhotosAlbum = UserData.GetAlbumByName(c_CoverPhotos);

            string coverPicUrl = string.Empty;

            if (coverPhotosAlbum != null)
            {
                coverPicUrl = coverPhotosAlbum.Photos[0].PictureNormalURL;
            }

            return coverPicUrl;
        }
    }
}
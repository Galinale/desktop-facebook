using System;
using System.Collections.Generic;
using System.Windows.Forms;
using FacebookWrapper.ObjectModel;

namespace FaceBookWPF.Models
{
    public class CircularItemDispatcher<T>
    {
        public Action<T> ItemDispatched; 

        private readonly Timer m_Timer;        
        private readonly IEnumerator<T> m_Iterator;

        private bool m_IsEnabled;
        public bool Enabled 
        { 
            get { return m_IsEnabled; }

            set 
            {
                m_IsEnabled = value;
                if(m_IsEnabled)
                {
                    m_Timer.Start();
                }
                else
                {
                    m_Timer.Stop();
                }

            }               
        } 

        public int Speed { get; set; }

        public CircularItemDispatcher(IEnumerable<T> i_UrlsList, int i_Speed = 5000)
        {
            Speed = i_Speed;

            m_Timer = new Timer { Interval = Speed };

            m_Iterator = i_UrlsList.GetEnumerator();
            m_Timer.Tick += (sender, e) =>
                                {
                                    m_Iterator.MoveNext();
                                    ItemDispatched(m_Iterator.Current);
                                };
        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using Caliburn.Micro;
using DesktopFacebook;
using FaceBookWPF.Models;
using FacebookWrapper.ObjectModel;
using Ninject;

namespace FaceBookWPF.ViewModels
{
    public class CollageViewModel : Screen , IPageCollage
    {
        private bool[] m_FriendsRemember; 

        [Inject]
        public CollageViewModel()
        {
            foreach (var friend in UserData.LoggedInUser.Friends)
            {
                Friends.Add(new SelectableFriendViewModel()
                                {
                                    FriendName = friend.Name,
                                    Picture = friend.PictureNormalURL
                                });
            }
        }

        private BindableCollection<SelectableFriendViewModel> m_Friends =
            new BindableCollection<SelectableFriendViewModel>();
        public BindableCollection<SelectableFriendViewModel> Friends
        {
            get { return m_Friends; }
            private set
            {
                m_Friends = value;
                NotifyOfPropertyChange(() => Friends);
            }
        }

        private string m_collageURI;
        public string CollageURI
        {
            get{ return m_collageURI;}
            set
            {
                m_collageURI = value;
                NotifyOfPropertyChange(() => CollageURI);
            }
        }

        //public bool CanCreateCollage()
        //{
        //    return getSelectedFriendsList().Any() ;
        //}

        public void CreateCollage()
        {
            var picURLs = getSelectedFriendsList()
                          .Select(selsectedFriend => selsectedFriend.Picture)
                          .ToList();

            CollageURI = Collage.CreateCollageOfProfilePics(picURLs);

        }

        //TODO: tweak the behaviour, maybe listen to each friends state.
        public bool IsAllChecked
        {
            get
            {
                return getSelectedFriendsList().Count() == Friends.Count;
            }
            set
            {
                bool isSelected = value;
                if (isSelected)
                {
                    storeFriendsSelectionState();
                    foreach (var friend in Friends)
                    {
                        friend.IsSelected = true;
                    }
                    
                }
                else
                {
                    restoreFriendsSelectionState();
                }

                NotifyOfPropertyChange(() => IsAllChecked);
            }
        }

        private IEnumerable<SelectableFriendViewModel> getSelectedFriendsList()
        {
            return from friend in Friends
                   where friend.IsSelected
                   select friend;
        }

        private void storeFriendsSelectionState()
        {
            m_FriendsRemember = new bool[Friends.Count];

            for (int i = 0; i < Friends.Count; i++)
            {
                m_FriendsRemember[i] =Friends[i].IsSelected;
            }
        }

        private void restoreFriendsSelectionState()
        {
            if (m_FriendsRemember != null)
            {
                for (int i = 0; i < m_FriendsRemember.Count(); i++)
                {
                    Friends[i].IsSelected = m_FriendsRemember[i];
                }
            }
        }
    }

}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Timers;
using System.Windows.Controls;
using Timer = System.Timers.Timer;

namespace DesktopFacebook.Views
{
    public partial class RunningTextBox : TextBox

    {
        private readonly Queue<char> m_QueuedText = new Queue<char>();
        private readonly Timer m_Timer = new Timer(150) { AutoReset = true };
        private IEnumerator<string> m_Iterator;
        private IEnumerable<string> m_MessageCollection; 

        public void SetText(string i_Message)
        {
            this.SetText(new List<string>()
            {
                i_Message
            });
        }

        public void SetText(IEnumerable<string> i_Messages)
        {
            if (this.m_Timer.Enabled)
            {
                this.m_Timer.Stop();
            }

            this.m_QueuedText.Clear();
            this.m_MessageCollection = i_Messages;
            this.m_Iterator = this.m_MessageCollection.GetEnumerator();
            this.Text = string.Empty;
            this.m_Timer.Start();
        }

        private void BreakTextToCharQueue(string i_Message)
        {
            for (int i = 0; i < i_Message.Length + this.MaxLength; i++)
            {
                char nextChar = (i < i_Message.Length) ? i_Message[i] : ' ';
                this.m_QueuedText.Enqueue(nextChar);
            }
        }

        public RunningTextBox()
        {
            this.InitializeComponent();
            this.myInitialize();
        }

        //public RunningTextBox(IContainer container)
        //{
        //    container.Add(this);
        //    this.InitializeComponent();
        //    this.myInitialize();
        //}

        private void myInitialize()
        {
            //this.ReadOnly = true;
            //this.RightToLeft = RightToLeft.No;
            this.TabIndex = 0;
            //this.TextAlign = HorizontalAlignment.Right;
            //this.Font = new Font(FontFamily.GenericMonospace, Font.Size);
            this.MaxLength = 100;//this.getMaxLength();

            this.m_MessageCollection = new List<string>() { "Empty" };
            this.m_Timer.Enabled = true;

            this.m_Timer.Elapsed += (object sender, ElapsedEventArgs args) =>
            {
                if (m_QueuedText.Count <= 0)
                {
                    if (m_Iterator != null && m_Iterator.MoveNext())
                    {
                        BreakTextToCharQueue(m_Iterator.Current);
                    }
                    else
                    {
                        m_Iterator = m_MessageCollection.GetEnumerator();
                    }
                }

                if (Text.Length > MaxLength)
                {
                    Text = Text.Substring(1);
                }

                if (m_QueuedText.Count > 0)
                {
                    Text += m_QueuedText.Dequeue();
                }
            };
        }

        //private void textAppender(char i_Char)
        //{
        //    MethodInvoker append = () => this.Text += i_Char;
        //    this.Invoke(append);
        //}

        //private void textSetter(string i_Str)
        //{
        //    MethodInvoker append = () => this.Text = i_Str;
        //    this.Invoke(append);
        //}

        //private int getTextLength()
        //{
        //    return (int)this.Invoke(
        //        new Func<int>(() => 
        //    {
        //        return this.Text.Length;
        //    }));
        //}

        //private int getMaxLength()
        //{
        //    char[] charArray = new char[100];
        //    string longStr = new string(charArray);

        //    Font f = Font;
        //    Rectangle rect = ClientRectangle;
        //    int charFitted;
        //    int linesFitted;
        //    using (Graphics g = CreateGraphics())
        //    {
        //        StringFormat sf = new StringFormat(StringFormatFlags.NoWrap);
        //        sf.LineAlignment = StringAlignment.Center;
        //        sf.Alignment = StringAlignment.Near;
        //        sf.Trimming = StringTrimming.EllipsisCharacter;
        //        g.MeasureString(longStr, f, rect.Size, sf, out charFitted, out linesFitted);     
        //    }

        //    return charFitted;
        //}
    }
}

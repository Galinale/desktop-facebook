﻿// -----------------------------------------------------------------------
// <copyright file="UserUtils.cs" company="Microsoft">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace DesktopFacebook
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using FacebookWrapper;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public static class UserUtils
    {
        public static bool IsUserLogged(this LoginResult LoginResult)
        {
            bool isNoErrorMsg = string.IsNullOrEmpty(LoginResult.ErrorMessage);
            bool hasAccessToken = LoginResult.AccessToken != null;
            return isNoErrorMsg && hasAccessToken;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FaceBookWPF
{
    public interface IPage
    {
    }

    public interface IPageLogin : IPage
    {

    }

    public interface IPageProfile : IPage
    {

    }

    public interface IPageCollage : IPage
    {
        
    }

    public interface IPost
    {
        
    }


    public interface IViewModelFactory
    {
        IPageLogin CreateLoginViewModel();
        IPageProfile CreateProfileViewModel();
        IPageCollage CreateCollageViewModel();

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Caliburn.Micro;

namespace FaceBookWPF.ViewModels
{
    public class SelectableFriendViewModel : Screen
    {

        public SelectableFriendViewModel(){}

        public SelectableFriendViewModel(string i_PictureURL ,string i_Name)
        {
            m_FriendName = i_Name;
            m_Picture = i_PictureURL;
        }

        private string m_Picture;
        public string Picture
        {
            get { return m_Picture; }
            set
            {
                m_Picture = value;
                NotifyOfPropertyChange(() => Picture);
            }
        }

        private string m_FriendName;
        public string FriendName
        {
            get { return m_FriendName; }
            set
            {
                m_FriendName = value;
                NotifyOfPropertyChange(() => FriendName);
            }
        }

        private bool m_IsSelected;
        public bool IsSelected
        {
            get { return m_IsSelected; }
            set 
            {
                m_IsSelected = value;
                NotifyOfPropertyChange(() => IsSelected); 
            }
        }
    }
}

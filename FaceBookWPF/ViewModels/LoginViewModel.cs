﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Caliburn.Micro;
using DesktopFacebook;
using DesktopFacebook.Models;
using FacebookWrapper.ObjectModel;
using Ninject;

namespace FaceBookWPF.ViewModels
{
    class LoginViewModel : Screen, IPageLogin
    {
        private readonly ILoginService m_LoginService;
        private readonly IEventAggregator m_EventAggregator;
   
        [Inject]
        public LoginViewModel( ILoginService i_LoginService , IEventAggregator i_EventAggregator)
        {
            m_EventAggregator = i_EventAggregator;
            m_LoginService = i_LoginService;

            m_CanLogin = true;
        }

        public bool RememberMe 
        {
            get { return m_LoginService.RememberMe; }

            set
            {
                m_LoginService.RememberMe = value;
                NotifyOfPropertyChange(() => RememberMe);
            }
        }

        private bool m_CanLogin;
        public bool CanLogin
        {
            get { return m_CanLogin; }
        }

        public void Login()
        {
            m_CanLogin = false;
            m_LoginService.Login();
            try
            {
                m_EventAggregator.Publish("Profile");
            }
            catch(Exception e )
            {
                throw new Exception("Login Error" , e);
            }          

        }
    }
}

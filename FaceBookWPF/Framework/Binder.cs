﻿using System.Collections.Generic;
using Caliburn.Micro;
using DesktopFacebook.Models;
using FaceBookWPF.ViewModels;
using Ninject.Extensions.Factory;
using Ninject.Modules;

namespace FaceBookWPF
{
    class Binder :NinjectModule
    {
        public override void Load()
        {

            //
            // FRAMEWORK
            //
            Bind<IWindowManager>().To<WindowManager>().InSingletonScope();
            Bind<IEventAggregator>().To<EventAggregator>().InSingletonScope();

            //
            // VIEW MODELS
            //
            Bind<ShellViewModel>().ToSelf().InSingletonScope();
            Bind<IPageLogin>().To<LoginViewModel>();
            Bind<IPageProfile>().To<ProfileViewModel>();
            Bind<IPageCollage>().To<CollageViewModel>();

            //
            // SERVICES
            //
            Bind<ILoginService>().To<LoginService>();

            //
            // FACTORIES
            //
            Bind<IViewModelFactory>().ToFactory();
        }
    }
}

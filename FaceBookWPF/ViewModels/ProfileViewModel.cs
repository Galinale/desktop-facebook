﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using Caliburn.Micro;
using DesktopFacebook;
using DesktopFacebook.Models;
using FaceBookWPF.Models;
using FaceBookWPF.Views;
using FacebookWrapper.ObjectModel;

namespace FaceBookWPF.ViewModels
{
    class ProfileViewModel :Conductor<IPage>.Collection.AllActive , IPageProfile
    {
        private readonly CircularItemDispatcher<string> m_Shuffler;
        private IEventAggregator m_EventAggregator;

        public ProfileViewModel(IEventAggregator i_EventAggregator)
        {

            m_EventAggregator = i_EventAggregator;
            var picUrls = from picture in UserData.GetAlbumByName("Profile Pictures").Photos
                                             select picture.URL;

            m_Shuffler = new CircularItemDispatcher<string>(picUrls);
            m_Shuffler.ItemDispatched += url => ProfilePic = url;
           
            ProfilePic =  UserData.LoggedInUser.PictureNormalURL;
            CoverPic = UserData.GetCoverPhotoUrl();


            

            m_Posts = new BindableCollection<PostViewModel>();

            var postViewBuilder = new PostViewBuilder();
            foreach (var post in UserData.GetUserPosts())
            {
                var postViewModel = new PostViewModel(post);

                postViewBuilder.CreateProfilePic();
                postViewBuilder.CreateName();
                postViewBuilder.CreateDate();
                postViewBuilder.CreateMessage();
                postViewBuilder.CreatePostPicture();
                postViewBuilder.CreateCaption();

                var postView123 = postViewBuilder.Build();
                ViewModelBinder.Bind(postViewModel, postView123, null);

                Posts.Add(postViewModel);
            }

            m_UpperPanel = new UpperPanelViewModel(m_EventAggregator);
            var upperPanelView = new UpperPanelView();
            ViewModelBinder.Bind(m_UpperPanel, upperPanelView, null);
            

            m_RunnungText = new RunningTextViewModel();
        }

        private UpperPanelViewModel m_UpperPanel;
        public UpperPanelViewModel UpperPanel
        {
            get { return m_UpperPanel; }
            set 
            {
                m_UpperPanel = value;
                NotifyOfPropertyChange(() => UpperPanel);
            }
        }

        private RunningTextViewModel m_RunnungText;
        public RunningTextViewModel RunningText
        {
            get { return m_RunnungText; }
            set
            {
                m_RunnungText = value;
                NotifyOfPropertyChange(() => RunningText);
            }
        }

        private string m_ProfilePic;
        public string ProfilePic
        {
            get { return m_ProfilePic; }
            set
            {
                m_ProfilePic = value;
                NotifyOfPropertyChange(() => ProfilePic);
            }
        }

        private string m_CoverPic;
        public string CoverPic
        {
            get { return m_CoverPic; }
            set
            {
                m_CoverPic = value;
                NotifyOfPropertyChange(() => CoverPic);
            }
        }

        private bool m_IsShuffle;
        public bool IsShuffle 
        { 
            get { return m_IsShuffle; }
            set
            {
                m_IsShuffle = value;
                m_Shuffler.Enabled = m_IsShuffle;
                NotifyOfPropertyChange(() => IsShuffle);
            }
        }

        private BindableCollection<PostViewModel> m_Posts;
        public BindableCollection<PostViewModel> Posts
        {
            get { return m_Posts; }
            set
            {
                m_Posts = value;
                NotifyOfPropertyChange(() => Posts);
            }
        }

        public void OpenCollageView()
        {
            m_EventAggregator.Publish("Collage");
        }


    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using FacebookWrapper;

namespace DesktopFacebook.Models
{

    public interface ILoginService
    {
        void Login();

        void Logout();

        bool RememberMe { get; set; }
    }

    class LoginService : ILoginService
    {
        public bool RememberMe
        {
            get { return Config.Instance.IsRememberMe; }
            set
            {
                Config.Instance.IsRememberMe = value;
                Config.Instance.Save();
            }
        }

        public void Login()
        {
            FacebookService.s_CollectionLimit = 100;
            LoginResult result = null;

            if (Config.Instance.IsRememberMe &&
                !string.IsNullOrEmpty(Config.Instance.AccessToken))
            {
                try
                {
                    Thread thread = new Thread(() => result = FacebookService.Connect(Config.Instance.AccessToken))
                    {
                        IsBackground = true
                    };

                    thread.Start();
                }
                catch (Exception)
                {
                    ////rollback for expired Access Token
                    result = logInAndSaveAccessKey();
                }
            }
            else
            {
                result = logInAndSaveAccessKey();
            }

            if (result.IsUserLogged())
            {
                UserData.LoggedInUser = result.LoggedInUser;
            }
        }

        public void Logout()
        {
            throw new NotImplementedException();
        }

        private static LoginResult logInAndSaveAccessKey()
        {
            LoginResult result;

            try
            {
                //TODO: Extract all the permissions to resources
                result = FacebookService.Login(Config.AppID,
                                               "user_about_me",
                                               "friends_about_me",
                                               "publish_stream",
                                               "user_events",
                                               "read_stream",
                                               "user_status",
                                               "user_photos",
                                               "user_photo_video_tags",
                                               "user_hometown",
                                               "user_likes");

                Config.Instance.AccessToken = result.AccessToken;
                Config.Instance.Save();
            }
            catch (Exception)
            {
                result = null;
            }

            return result;
        }
    }
}

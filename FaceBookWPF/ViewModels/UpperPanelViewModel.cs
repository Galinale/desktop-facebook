﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Caliburn.Micro;

namespace FaceBookWPF.ViewModels
{
    public class UpperPanelViewModel : Screen ,IPage
    {
        private readonly IEventAggregator m_EventAggregator;

        public UpperPanelViewModel(IEventAggregator i_EventAggregator)
        {
            m_EventAggregator = i_EventAggregator;
        }

        public void LogOut()
        {
            m_EventAggregator.Publish("Login");
            
        }

        public bool CanGoToProfilePage()
        {
            return !(Parent.GetType() == typeof (ProfileViewModel));
        }

        public void GoToProfilePage()
        {
            m_EventAggregator.Publish("Profile");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Text;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using FaceBookWPF.Properties;

namespace DesktopFacebook
{
    public static class FacebookFontProvider
    {
        [DllImport("gdi32.dll")]
        private static extern IntPtr AddFontMemResourceEx(IntPtr pbFont, uint cbFont, IntPtr pdv, [In] ref uint pcFonts);

        private static FontFamily m_fontfamily;
        
        private static Font m_LargeFont = null;

        public static Font LargeFont
        {
             get
             {
                 if (m_LargeFont == null)
                 {
                       LoadPrivateFontCollection(out m_LargeFont, 15F);
                 }

                 return m_LargeFont;
             }
        }

        private static Font m_NormallFont = null;

        public static Font NormalFont
        {
            get
            {
                if (m_NormallFont == null)
                {
                    LoadPrivateFontCollection(out m_NormallFont, 8.25F);
                }

                return m_NormallFont;
            }
        }

        private static void LoadPrivateFontCollection(out Font i_Font, float i_size)
        {
            //// Create the byte array and get its length

            byte[] fontArray = Resources.FacebookFont;
            int dataLength = Resources.FacebookFont.Length;

            //// ASSIGN MEMORY AND COPY  BYTE[] ON THAT MEMORY ADDRESS
            IntPtr ptrData = Marshal.AllocCoTaskMem(dataLength);
            Marshal.Copy(fontArray, 0, ptrData, dataLength);

            uint cFonts = 0;
            AddFontMemResourceEx(ptrData, (uint)fontArray.Length, IntPtr.Zero, ref cFonts);

            PrivateFontCollection pfc = new PrivateFontCollection();
            ////PASS THE FONT TO THE  PRIVATEFONTCOLLECTION OBJECT
            pfc.AddMemoryFont(ptrData, dataLength);

            ////FREE THE  "UNSAFE" MEMORY
            Marshal.FreeCoTaskMem(ptrData);

            m_fontfamily = pfc.Families[0];
            i_Font = new Font(m_fontfamily, i_size, FontStyle.Regular);
        }
    }
}

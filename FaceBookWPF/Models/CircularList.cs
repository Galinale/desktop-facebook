﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FaceBookWPF.Models
{

    //
    //  PROXY OF LIST
    //
    public class CircularList<T> : List<T>
    {
        private Iter m_Iter = null;

        public new IEnumerator<T> GetEnumerator()
        {
            if(m_Iter == null)
            {
                m_Iter = new Iter(this);
            }

            return m_Iter;
        }

        private class Iter : IEnumerator<T>
        {
            private readonly List<T> m_Items;
            private int m_Index;

            public Iter(List<T> i_List)
            {
                m_Items = i_List;
                m_Index = -1;
            }

            public bool MoveNext()
            {
                if (m_Index == m_Items.Count - 1)
                {
                    m_Index = -1;
                }

                m_Index++;
                return true;
            }

            public void Reset()
            {
                m_Index = -1;
            }

            public T Current
            {
                get { return m_Items[m_Index]; }
            }

            object IEnumerator.Current
            {
                get { return Current; }
            }

            public void Dispose() { }
        }
    }



}